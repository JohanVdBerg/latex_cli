\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{mycls}[2011/03/26 My custom CV class]
\LoadClass{article}
\RequirePackage{kvoptions}
\SetupKeyvalOptions{ 
%	family=MCS,
	prefix=MCS@
}

\DeclareStringOption{author}
%\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}

\ProcessKeyvalOptions*

%%%%%%%%%%%%%%%%%%%% page size
\usepackage[paperwidth=210mm, paperheight=297mm,vmargin=2cm,hmargin=1cm,showframe]{geometry}

%%%%%%%%%%%%%%%%%%%%% tables
\RequirePackage{xltabular}

%%%%%%%%%%%% git info
\RequirePackage{gitver}

%%%%%%%%%%%% PDF
\RequirePackage{hyperxmp}
\RequirePackage{hyperref}


\newcommand\myabstract{Git information: \gitVer}

\hypersetup{%
	breaklinks=true,
	hyperindex=true,
	colorlinks=false,
	hidelinks=true,
	pdftitle=title,
	pdfauthor=\MCS@author,
	pdfsubject=subject,
	pdfsubject={\myabstract}
}

% Attach file
\RequirePackage{embedfile}

%FONT

\RequirePackage{fontspec}
\usepackage{Acorn, AnnSton, ArtNouv, ArtNouvc, Carrickc, Eichenla, Eileen, EileenBl, Elzevier, GotIn, GoudyIn, Kinigcap, Konanur, Kramer, MorrisIn, Nouveaud, Romantik, Rothdn, Royal, Sanremo, Starburst, Typocaps, Zallman}
\usepackage{auncial,aurical,pifont,adforn}
%\setmainfont{Arial}
%\setsansfont{TeX Gyre Heros}
%\setmainfont{Arial}  