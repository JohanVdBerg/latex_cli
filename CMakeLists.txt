cmake_minimum_required(VERSION 3.9)
project(hello_cpp_docker) # Set our project to use C++ 17
set(CMAKE_CXX_STANDARD 17) # Set our source files as just the main.cpp
set(SOURCE_FILES main.cpp) # Create our executable file from our source files
add_executable(gen_data ${SOURCE_FILES}) 

set(TEX_DOC_BASE document)

add_custom_command(
    TARGET gen_data
    POST_BUILD
    COMMAND gen_data
    COMMAND latexmk -pdflatex=lualatex -pdf -shell-escape ${TEX_DOC_BASE}.tex
    BYPRODUCTS ${TEX_DOC_BASE}.pdf
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    VERBATIM)



# https://gitlab.kitware.com/cmake/community/-/wikis/FAQ#how-do-i-use-cmake-to-build-latex-documents
# https://cmake.org/cmake/help/latest/command/add_custom_command.html


 